﻿using Course_App.Models;
using Course_App.Repository;
using System;

namespace Course_App
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Creating a CourseRepository object with custom array size
            CourseRepository manager = new CourseRepository();

            // Creating a Course object
            Course course1 = new Course() { Name="DotNet",Price=2000, Description="Learn DotNet",Category="IT",Duration=20};
            Course course2 = new Course() { Name = "java", Price = 2000, Description = "Learn DotNet", Category = "IT", Duration = 20 };
            Course course3 = new Course() { Name = "python", Price = 2000, Description = "Learn DotNet", Category = "IT", Duration = 20 };
            manager.AddCourse(course1);
            manager.AddCourse(course2);
            manager.AddCourse(course3);

            Console.WriteLine(manager.GetCourseCountNotNull());

            manager.DeleteCourseByName(course2.Name);

            Console.WriteLine(manager.GetCourseCountNotNull());
            Course[] course = manager.GetAllCoursesNotNull();
            foreach (Course c in course)
            {
                Console.WriteLine(c);
            }

        }
    }
}
