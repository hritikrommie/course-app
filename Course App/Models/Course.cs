﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_App.Models
{
    public class Course
    {
        #region Properties
        public string Name  { get; set; }
        public double Price { get; set; }   
        public string Description { get; set; } 
        public string Category { get; set; }
        public double Duration { get; set; }

        #endregion

        public override string ToString()
        {
            return $"Course Name : {Name}\n" +
                $"Course Price : Rs. {Price}\n" +
                $"Course Description : {Description}\n" +
                $"Course Category : {Category}\n" +
                $"Course Duration : {Duration} hour\n";
        }
    }
}
