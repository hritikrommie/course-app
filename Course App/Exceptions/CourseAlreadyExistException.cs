﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_App.Exceptions
{
    public class CourseAlreadyExistException:ApplicationException
    {
        public CourseAlreadyExistException()
        {
                
        }
        public CourseAlreadyExistException(string message):base(message)
        {

        }
    }
}
