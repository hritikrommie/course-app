﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_App.Exceptions
{
    public class CourseDoesNotExistException:ApplicationException
    {
        public CourseDoesNotExistException()
        {
                
        }
        public CourseDoesNotExistException(string message):base(message)    
        {

        }
    }
}
