﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_App.Exceptions
{
    public class CourseLimitReachedException:ApplicationException
    {
        public CourseLimitReachedException()
        {

        }
        public CourseLimitReachedException(string message):base(message)
        {

        }
    }
}
