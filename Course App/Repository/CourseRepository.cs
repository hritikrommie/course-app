﻿using Course_App.Exceptions;
using Course_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_App.Repository
{
    public class CourseRepository : ICourseRepository
    {
        Course[] courses;

        #region Constructors

        // No arg constructor
        // Makes a course array of size 10
        public CourseRepository()
        {
            courses = new Course[10];
        }

        // Parametrized constructor
        // Makes a courses array of custom size 
        public CourseRepository(int capacity)
        {
            courses = new Course[capacity];
        }

        #endregion

        // Add a new course to courses array
        public void AddCourse(Course newCourse)
        {
            int courseCount = GetCourseCountNotNull();
            try 
            {
                if(courseCount == courses.Length)
                {
                    throw new CourseLimitReachedException("Can't add more courses. Limit Reached!");
                }
                
                Course sameNameCourse = GetCourseByName(newCourse.Name);
                if(sameNameCourse != null)
                {
                    throw new CourseAlreadyExistException($"Course with name {newCourse.Name} already exist.");
                }
                else
                {
                    for(int i = 0; i < courses.Length; i++)
                    {
                        if (courses[i] == null)
                        {
                            courses[i] = newCourse;
                            break;
                        }
                    }
                }
            }
            catch(CourseLimitReachedException clrx)
            {
                Console.WriteLine(clrx.Message);
            }
            catch (CourseAlreadyExistException caex)
            {
                Console.WriteLine(caex.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        // Delete a course by name
        public Course DeleteCourseByName(string name)
        {
            Course foundCourse = null;
            for(int i = 0; i < courses.Length; i++)
            {
                if (courses[i] != null && courses[i].Name == name)
                {
                    foundCourse = courses[i];
                    courses[i] = null;
                    break;
                }
            }
            try
            {
                if (foundCourse == null)
                    throw new CourseDoesNotExistException($"Course with name {name} does not exist.");
            }
            catch(CourseDoesNotExistException cdnex)
            {
                Console.WriteLine(cdnex.Message);
            }
            return foundCourse;
        }


        // Get an array containing all courses
        public Course[] GetAllCoursesNotNull()
        {
            int totalCourse = GetCourseCountNotNull();
            Course[] returnCourses = new Course[totalCourse];
            int i = 0;
            foreach(Course course in courses)
            {
                if(course != null)
                {
                    returnCourses[i++] = course;
                }
            }
            return returnCourses;
        }


        // Get All courses in a category
        public Course[] GetAllCourseByCategory(string category)
        {
            int courseInCategory = GetCourseCountByCategory(category);
            Course[] foundCourses  = new Course[courseInCategory];
            int i = 0;
            foreach(Course course in courses)
            {
                if(course.Category == category)
                {
                    foundCourses[i++] = course;
                }
            }
            return foundCourses;
        }

        // Get a course by name
        public Course GetCourseByName(string name)
        {
            Course foundCourse = null;
            foreach(Course course in courses)
            {
                if(course != null && course.Name == name)
                {
                    foundCourse = course;
                    break;
                }
            }
            
            return foundCourse;
        }

        // Get course count
        public int GetCourseCountNotNull()
        {
            int courseCount = 0;
            foreach (Course course in courses)
            {
                if(course != null)
                {
                    courseCount++;
                }
            }
            return courseCount;
        }

        // Get number of courses of a given category
        public int GetCourseCountByCategory(string category)
        {
            int courseCountInCategory = 0;
            foreach (Course course in courses)
            {
                if (course.Category == category)
                {
                    courseCountInCategory++;
                }
            }
            return courseCountInCategory;
        }

        public void UpdateCourseCategory(string name, string newCategory)
        {
            Course courseToBeUpdated = GetCourseByName(name);
            courseToBeUpdated.Category = newCategory;
        }

        public void UpdateCourseDescription(string name, string newDescription)
        {
            Course courseToBeUpdated = GetCourseByName(name);
            courseToBeUpdated.Description = newDescription;
        }

        public void UpdateCourseDuration(string name, double newDuration)
        {
            Course courseToBeUpdated = GetCourseByName(name);
            courseToBeUpdated.Duration = newDuration;
        }

        public void UpdateCourseName(string oldName, string newName)
        {
            Course courseToBeUpdated = GetCourseByName(oldName);
            courseToBeUpdated.Name = newName;
        }

        public void UpdateCoursePrice(string name, double newPrice)
        {
            Course courseToBeUpdated = GetCourseByName(name);
            courseToBeUpdated.Price = newPrice; 
        }
    }
}
