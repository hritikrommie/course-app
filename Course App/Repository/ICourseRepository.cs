﻿using Course_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_App.Repository
{
    internal interface ICourseRepository
    {
        // Add Course Functionalities
        void AddCourse(Course newCourse);

        // Delete Course
        Course DeleteCourseByName(string name);

        // Update Course Functionalities
        void UpdateCourseName(string oldName, string newName);
        void UpdateCourseDescription(string name, string newDescription);
        void UpdateCoursePrice(string name, double newPrice);
        void UpdateCourseCategory(string name, string newCategory);
        void UpdateCourseDuration(string name, double newDuration);

        // Get courses Functionalities
        Course[] GetAllCoursesNotNull();
        Course GetCourseByName(string name);
        Course[] GetAllCourseByCategory(string category);

        // Get course Count Functionalities
        int GetCourseCountNotNull();
        int GetCourseCountByCategory(string category);
    }
}
