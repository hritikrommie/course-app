# Course App

### Interface
* ICourseRepository
![Interface](image\Interface.png)

### Functionalities
* CourseRepository
![Interface](image\CourseRepoConstr.png)

* AddCourse 
![Interface](image\addCourse.png)

* DeleteCourseByName
* GetAllCoursesNotNull
![Interface](image\deleteCourse.png)

* GetAllCourseByCategory
* GetCourseByName
* GetCourseCountNotNull
![Interface](image\getAllCourse.png)

* GetCourseCountByCategory
* UpdateCourseCategory
* UpdateCourseDescription
* UpdateCourseName
* UpdateCoursePrice
* UpdateCourseDuration
![Interface](image\update.png)